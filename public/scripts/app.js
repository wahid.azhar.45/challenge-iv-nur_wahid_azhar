class App {
    constructor() {
        this.img_car = document.getElementById('img_car');
        this.cb_tipe_driver = document.getElementById('cb_tipe_driver');
        this.cb_tipe_driver = document.getElementById('cb_tipe_driver');
        this.cb_time_picker = document.getElementById('cb_time_picker');
        this.txt_jumlah_penumpang = document.getElementById('txt_jumlah_penumpang');
        this.btn_cari_mobil = document.getElementById('btn_cari_mobil');
        this.data_container = document.getElementById('data_container');
        this.card_mobil = document.getElementById('card_mobil');
        this.img_card = document.getElementById('img_card');
        this.txt_nama_mobil = document.getElementById('txt_nama_mobil');
        this.txt_harga_mobil = document.getElementById('txt_harga_mobil');
        this.txt_deskripsi_mobil = document.getElementById('txt_deskripsi_mobil');
        this.txt_keterangan_penumpang = document.getElementById('txt_keterangan_penumpang');
        this.txt_keterangan_setting = document.getElementById('txt_keterangan_setting');
        this.txt_keterangan_waktu = document.getElementById('txt_keterangan_waktu');
    }

    async init() {
        await this.load();

        // Register click listener
        // this.clearButton.onclick = this.clear;
        this.btn_cari_mobil.onclick = this.run;
    }

    run = () => {
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.innerHTML = car.render();
            this.card_mobil.appendChild(node);
        });
    };

    async load() {
        const cars = await Binar.listCars();
        Car.init(cars);
    }

    clear = () => {
        let child = this.card_mobil.firstElementChild;

        while (child) {
            child.remove();
            child = this.card_mobil.firstElementChild;
        }
    };
}

module.exports = App