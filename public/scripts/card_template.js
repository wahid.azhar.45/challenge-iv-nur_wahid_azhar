function showCard(carImage, carType, carRent, carDescription, carCapacity, carTransmission, carYear) {
    var myHTML = `
            <div id="card_mobil" class="card ms-3 mt-3" style="width: 18rem;">
                <div class="card-body position-relative" style="height: 530px;">
                    <p>
                        <img id="img_card" src="${carImage}" height="160px" width="255px">
                    </p>
                    <h6 id="txt_nama_mobil">
                        ${carType}
                    </h6>
                    <h5 id="txt_harga_mobil" class="card-title">${carRent} / hari</h5>
                    <p id="txt_deskripsi_mobil" class="card-text">${carDescription}
                    </p>
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <i class="fa-solid fa-user-group"></i>
                        </div>
                        <div id="txt_keterangan_penumpang" class="ms-3">
                            ${carCapacity} Orang
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-2">
                        <div>
                            <i class="fa-solid fa-gear"></i>
                        </div>
                        <div id="txt_keterangan_setting" class="ms-3">
                            ${carTransmission}
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <div>
                            <i class="fa-solid fa-calendar"></i>
                        </div>
                        <div id="txt_keterangan_waktu" class="ms-3">
                            Tahun ${carYear}
                        </div>
                    </div>
                    <div style="position: absolute; bottom: 15px; width: 89%;">
                        <a class="btn btn-success p-2" style="width: 100%;">Pilih Mobil</a>
                    </div>
                </div>
            </div>
            `
    return myHTML
}